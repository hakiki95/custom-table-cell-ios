//
//  ViewController.swift
//  CustomTableCell
//
//  Created by Rimbunesia on 3/11/18.
//  Copyright © 2018 Rimbunesia. All rights reserved.
//

import UIKit

struct dataBuah {
    let name : String!
    let vit : String!
}


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var dataTable: UITableView!
    
    var data : [dataBuah] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        data =
            [
                dataBuah(name : "Mango", vit: "C"),
                dataBuah(name : "Apple", vit: "B"),
                dataBuah(name : "Penapple", vit: "C"),
                dataBuah(name : "Dragon Fruit", vit: "A"),
                dataBuah(name : "Popaye", vit: "A")
            ]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ceil = Bundle.main.loadNibNamed("CellNewsTableViewCell", owner: self, options: nil)?.first as! CellNewsTableViewCell
        ceil.labelBuah.text = data[indexPath.row].name
        ceil.labelVitamin.text = data[indexPath.row].vit
        
        return ceil
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 61
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("item disilect " + data[indexPath.row].name)
    }


}

